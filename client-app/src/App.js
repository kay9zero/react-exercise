import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

// React Bootstrap Components
import Container from 'react-bootstrap/Container';

// Sibling Components
import Search from './Search';

function App() {
  return (
    <div className="App">
      <Container>
        <h1>Who's My Representative</h1>
        <Search />
      </Container>
    </div>
  );
}

export default App;
