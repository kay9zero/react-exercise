import React from 'react';

// React Bootstrap Components
import Container from 'react-bootstrap/Container';
import Row       from 'react-bootstrap/Row';
import Col       from 'react-bootstrap/Col';
import Table     from 'react-bootstrap/Table';

// Sibling Components
import SearchResultsTitle from './SearchResultsTitle';
import PersonInfo         from './PersonInfo';

class SearchResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      personInfo: null
    };
  }

  render() {
    var results = this.props.results;
    if (results.length === 0) {
      return null;
    }

    return (
      <div className="SearchResults" style={{marginTop: "1em"}}>
        <Container>
          <Row>
            <Col>
              <SearchResultsTitle label={this.props.label} />
              <Table>
                <thead style={{backgroundColor:"#f0f0f0"}}>
                  <tr>
                    <th>Name</th>
                    <th>Party</th>
                  </tr>
                </thead>
                <tbody>
                  {results.map(
                    (person, i) =>
                      <tr key={i} onClick={ () => this.setState({ personInfo: person }) }>
                        <td>{person.name}</td>
                        <td>{person.party}</td>
                      </tr>
                  )}
                </tbody>
              </Table>
            </Col>
            <Col>
              <PersonInfo person={this.state.personInfo} />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default SearchResults;
