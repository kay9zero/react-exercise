import React from 'react';

// React Bootstrap Components
import Container from 'react-bootstrap/Container';
import Row       from 'react-bootstrap/Row';
import Col       from 'react-bootstrap/Col';
import Form      from 'react-bootstrap/Form';
import Button    from 'react-bootstrap/Button';

// Sibling Components
import SearchResults from './SearchResults';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      usState: null,
      searchBy: "representatives",
      resultsLabel: '',
      results: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({
      [name]: target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const searchBy = this.state.searchBy;
    const usState  = this.state.usState;

    this.setState({
      resultsLabel: searchBy
    });

    //TODO config host and port!
/*
    // localhost development, clean this up sometime later; Comment out for meow...
    const host = 'localhost';
    const port = 9930;

    const url      = 'http://' + host + ':' + port + '/' + searchBy + '/' + usState;
*/
    const url      = '/fetch-rep-api/' + searchBy + '/' + usState;

    fetch(url)
      .then(data => data.json())
      .then(data => this.setState({results: data.results}))
  }

  render() {
    return (
      <div className="Search">
        <Container>
          <Row>
            <Col>
              <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="formTextUsState">
                  <Form.Control
                    required
                    type="text"
                    name="usState"
                    placeholder="US State"
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Check
                    required
                    inline
                    type="radio"
                    id="inlineRadioRepresentatives" 
                    name="searchBy"
                    label="Representatives"
                    value="representatives"
                    onChange={this.handleChange}
                  />

                  <Form.Check
                    required
                    inline
                    type="radio"
                    id="inlineRadioSenators" 
                    name="searchBy"
                    label="Senators"
                    value="senators"
                    onChange={this.handleChange}
                  />
                </Form.Group>

                <Button
                  variant="primary"
                  type="submit"
                  size="sm"
                >
                  Search
                </Button>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col>
              <SearchResults results={this.state.results} label={this.state.resultsLabel}/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Search;
