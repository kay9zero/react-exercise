import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders title "Who\'s My Representative"', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Who's My Representative/i);
  expect(linkElement).toBeInTheDocument();
});
