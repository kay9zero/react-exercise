import React from 'react';

class SearchResultsTitle extends React.Component {
  capitalize(str) {
    if (typeof str !== 'string') return ''
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  render() {
    return (
      <div className="SearchResultsTitle">
        <h3>List / {this.capitalize( this.props.label )}</h3>
      </div>
    );
  }
}

export default SearchResultsTitle;
